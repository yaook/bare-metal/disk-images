#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

set -euxo pipefail

trap 'cleanup $?' EXIT

LVM="${LVM:-true}"
LUKS="${LUKS:-true}"

sourceld=""
targetld=""
sourcemnt=""
targetmnt=""

convert() {
    infile=$1
    outfile=$2

    # Copy the GPT and the first two partitions (EFI and legacy grub) to the new Image
    dd if="${infile}" of="${outfile}" bs=512 count="$(sgdisk -i 3 "${infile}"|grep -oP '(?<=^First sector: )\d+')"

    # Extend image to the size of the source image plus 500 MiB for the new boot partition
    truncate -r "${infile}" -s '+500M' "${outfile}"

    # restore secondary GPT header at the new end of the image
    sgdisk -e "${outfile}"

    # replace the root partition with a raid partition using the extra space
    sgdisk -d 3 -N 3 -t 3:fd00 -c 3:raid "${outfile}"

    # attach image to loop device and create raid
    targetld=$(losetup --show -f -P "${outfile}")
    mdadm --create /dev/md127 --level 1 --raid-disks 1 --metadata 1.2 --run --force "${targetld}p3"
    sgdisk -o /dev/md127

    # mount source
    sourceld=$(losetup --show -f -P "${infile}")
    sourcemnt=$(mktemp -d)
    mount "${sourceld}p3" "${sourcemnt}"

    # keep root label, so we don't have to change it in the grub config.
    # (without a short wait, lsblk may output empty UUIDs and labels for new devices)
    sleep 1s
    rootlabel="$(lsblk -no LABEL "${sourceld}p3")"

    # make a temporary fstab
    fstab="$(mktemp)"
    echo "LABEL=${rootlabel} / ext4 defaults 0 1" >> "${fstab}"

    targetmnt=$(mktemp -d)
    if [[ "${LVM}" == "true" ]]; then
        # create boot partition
        sgdisk -n '1:0:+500M' -t 1:bc13c2ff-59e6-4262-a352-b275fd6f7172 -c 1:boot /dev/md127

        if [[ "${LUKS}" == "true" ]]; then
            sgdisk -N 2 -t 2:ca7d7ccb-63ed-4c53-861c-1742536059cc /dev/md127
            kpartx /dev/md127
            read passphrase < <(dd if=/dev/urandom bs=24 count=1|base64)
            cryptsetup --type luks2 luksFormat /dev/md127p2 - <<< "${passphrase}"
            cryptsetup --type luks2 open /dev/md127p2 md127p2_crypt --key-file=- <<< "${passphrase}"
            pv=/dev/mapper/md127p2_crypt
            cryptuuid="$(blkid --probe --match-tag UUID --output value /dev/md127p2)"

        else
            sgdisk -N 2 -t 2:e6d6d379-f507-44c2-a23c-238f2a3df928 /dev/md127
            kpartx /dev/md127
            pv=/dev/md127p2
        fi

        # create volume group with PV
        pvcreate "${pv}"
        vgcreate vg0 "${pv}"

        # create, format, and mount root LV
        lvcreate --type linear --extents '100%VG' --name root vg0
        mkfs.ext4 -L "${rootlabel}" /dev/mapper/vg0-root
        mount /dev/mapper/vg0-root "${targetmnt}"

        # format and mount boot partition and add to fstab
        mkfs.ext4 -L boot /dev/md127p1
        mkdir "${targetmnt}/boot"
        mount /dev/md127p1 "${targetmnt}/boot"
        echo "LABEL=boot /boot ext4 defaults 0 1" >> "${fstab}"

    else
        # create new root partition, format and mount it
        sgdisk -N 1 -t 1:8300 -c 1:root /dev/md127
        kpartx /dev/md127
        mkfs.ext4 -L "${rootlabel}" /dev/md127p1
        mount /dev/md127p1 "${targetmnt}"

    fi

    # add EFI partition to fstab
    echo "LABEL=MKFS_ESP /boot/efi vfat defaults 0 1" >> "${fstab}"

    # copy contents
    cp -a "${sourcemnt}/." "${targetmnt}"

    # copy new fstab
    cp "${fstab}" "${targetmnt}/etc/fstab"

    # Disable bootloader spec to have grub-mkconfig generate the bootloader config
    echo 'GRUB_ENABLE_BLSCFG=false' >> "${targetmnt}/etc/default/grub"

    # osprober sometimes does not remove dm-devices it creates, preventing cleanup. Also we don't need it.
    echo 'GRUB_DISABLE_OS_PROBER=true' >> "${targetmnt}/etc/default/grub"

    # Grub is already installed, both as EFI binary and MBR/grub-partion,
    # so we leave it alone and just chroot into the image to rebuild grub.cfg.
    mount "${targetld}p1" "${targetmnt}/boot/efi"
    mount -o bind /dev "${targetmnt}/dev"
    mount -o bind /proc "${targetmnt}/proc"
    mount -o bind /sys "${targetmnt}/sys"
    chroot "${targetmnt}" grub-mkconfig -o /boot/grub/grub.cfg
    chroot "${targetmnt}" grub-install --target=x86_64-efi --recheck --removable --efi-directory=/boot/efi --boot-directory=/boot "${targetld}"
    chroot "${targetmnt}" grub-install --target=x386-pc "${targetld}"

    if [[ "${LUKS}" == "true" ]]; then
        # write key file and crypttab into the target filesystem
        echo "${passphrase}" > "${targetmnt}/etc/luks-keyfile"
        chmod 600 "${targetmnt}/etc/luks-keyfile"
        echo "md127p2_crypt UUID=${cryptuuid} /etc/luks-keyfile luks,discard" > "${targetmnt}/etc/crypttab"
        mkdir -p "${targetmnt}/etc/cryptsetup-initramfs/"
        echo 'KEYFILE_PATTERN="/etc/luks-keyfile"' >>"${targetmnt}/etc/cryptsetup-initramfs/conf-hook"
        echo 'UMASK=0077' >>"${targetmnt}/etc/initramfs-tools/initramfs.conf"
        # regenerate the initramfs of the first kernel entry in grub.cfg to include crypttab and the keyfile
        #kernelversion="$(grep -oPm 1 '(?<=\tlinux\t/vmlinuz-)\S+' "${targetmnt}/boot/grub/grub.cfg")"
        #chroot "${targetmnt}" dracut --force --add 'crypt lvm mdraid' --install '/etc/crypttab /etc/luks-keyfile cryptsetup' "/boot/initramfs-${kernelversion}.img" "${kernelversion}"
        chroot "${targetmnt}" /sbin/update-initramfs -c -k all
    fi

    # copy raid inflation script into the image
    # override growroot which is invoked by the image already
    cp "$(dirname $BASH_SOURCE)/inflate.py" "${targetmnt}/usr/local/sbin/growroot"
    chown root:root "${targetmnt}/usr/local/sbin/growroot"
    chmod +x "${targetmnt}/usr/local/sbin/growroot"
    sync
}

cleanup() {
    if [[ "$1" != "0" ]]; then
        echo "Error: last command returned $1. Attempting cleanup ..."
    fi

    set +e

    if [[ ! -z "${sourcemnt}" ]]; then
        umount "${sourcemnt}"
        rmdir "${sourcemnt}"
    fi

    if [[ ! -z "${targetmnt}" ]]; then
        umount "${targetmnt}/sys"
        umount "${targetmnt}/proc"
        umount "${targetmnt}/dev"
        umount "${targetmnt}/boot/efi"
        umount "${targetmnt}/boot"
        # blkdeactivate recursively unmounts and deactivates any dm devices on the raid, as well as the raid itself.
        # Unfortunately, it sometimes seems to lose cached writes on stacked dm-devices, so we call sync first.
        sync
	sync
	sync
        blkdeactivate -u /dev/md127
        rmdir "${targetmnt}"
    fi

    if [[ ! -z "${sourceld}" ]]; then
        losetup -d "${sourceld}"
    fi

    if [[ ! -z "${targetld}" ]]; then
        losetup -d "${targetld}"
    fi

    exit $1
}

convert $1 $2

exit 0
