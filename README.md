<!--
Copyright (c) 2021 The Yaook Authors.

This file is part of Yaook.
See https://yaook.cloud for further info.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

# disk-images

This repository is a collection of scripts used to build disk- and ramdisk-images that may be used with the Ironic service when deploying Yaook to baremetal.

## Dependencies

The current scripts use diskimage-builder, which can be installed via pip:

    pip3 install diskimage-builder

If you want to build IPA images, there is a custom diskimage-builder element that you need to install as well:

    pip3 install ironic-python-agent-builder

diskimage-builder manages its binary dependencies with bindep (https://github.com/openstack/diskimage-builder/blob/master/bindep.txt).

However, some dependencies are not available on all platforms. The `centos-minimal` element used by `build-ipa.sh` requires the `yum-utils` package,
which is not available on ubuntu releases newer than 18.04.

`build_centos8raid.sh` additionally requires
* gdisk
* kpartx
* mdadm
* cryptsetup
* lvm2

## Usage

Runing any of the `scripts/build_*.sh` scripts will create the respective image files in the current directory.
In case of `build_ipa.sh` this will be the kernel and ramdisk files, otherwise it will be a qcow2 image with md5 and sha256 files.

The build scripts require root privileges to manipulate loop, raid, crypt, and lvm devices.

## SSH Access

The scripts will add ssh public keys from an `authorized_keys` file in the local directory to the `centos` user in the images.


## RAID Image

The image produced by `build_centos8raid.sh` contains mdraid metadata for a single-disk RAID 1 array which holds a LUKS-encrypted LVM containing the root filesystem as a logical volume.
Setting the `LUKS` and `LVM` environment variables to `false` will cause the Image to be built with a root fs partition directly on the raid device.
This setup allows a second disk to be added at runtime, creating a proper RAID 1 setup without the need to reboot the system or the requirement for Ironic to set it up correctly.

For this purpose, A script is included in the image under `/usr/local/bin/inflate.py`.
The script will try to find a disk of the same size as the current bootdisk and duplicate the bootdisks partitions onto it.
It will add the new raid partition to the active raid of the bootdisk and install a bootloader to the new disk so that the node is capable of booting from both disks, should one of them fail.
It will also resize the RAID partitions and the root partition/LV to fill the available size and reencrypt LUKS devices to avoid reusing encryption keys on multiple devices.

Currently `inflate.py` does not run automatically, it has to be triggered via cloud-init or run manually.

For this image to boot correctly, the disks of the host should be cleared of any partitions and raid metadata from previous installations.
A standard IPA image will automatically activate any RAID devices found on the disks, causing Ironics standard `erase_devices_metadata` cleaning step to fail.
It is recommended to use an image created with the `build_ipa.sh` script which disables automatic raid detection.

## License

[Apache 2](LICENSE.txt)
